from django.conf.urls import url

from . import views

app_name = 'quest'
urlpatterns = [
    url(r'^$', views.get_urlbuilder, name='index'),
    url(r'^(?P<source_id>.+)/results/$', views.get_results, name='results'),
    url(r'^analysis/$', views.get_analysis, name='analysis'),
    url(r'^mail/$', views.get_mail, name='mail'),
    url(r'^contact/$', views.get_contact, name='contact'),
    url(r'^projects/$', views.get_projects, name='projects'),
    url(r'^404/$', views.get_404, name='404'),
]
