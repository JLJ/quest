import ast
import json
import pandas
import newspaper

from django.urls import reverse
from django.shortcuts import render
from django.core.mail import send_mail
from django.http import HttpResponseRedirect

from urllib.request import urlopen
from textblob import TextBlob

from .forms import ContactForm, URLBuilderForm, ArticleForm, EmailForm


def get_urlbuilder(request):
    """
    This view method gets called on name url index
    If GET, URLBuilderForm is rendered on index.html
    If POST, building_source_url method is called and list of sources rendered on sources.html
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = URLBuilderForm(request.POST)
        # check whether it is valid
        if form.is_valid():

            q1 = form.cleaned_data['q1']
            q2 = form.cleaned_data['q2']
            q3 = form.cleaned_data['q3']

            # if q1, q2 or q3 set to all, make them empty
            if q1 == 'all':
                q1 = ''
            if q2 == 'all':
                q2 = ''
            if q3 == 'all':
                q3 = ''

            # build url accrding to params selected
            sources = building_source_url(q1, q2, q3)

            # if no sources returned with selected parameters, render index.html with error
            if len(sources['sources']) < 1:
                # return render(
                #     request,
                #     'quest/index.html',
                #     {
                #         'form': form,
                #         'error': "*Your inputs resulted in 0 news sources. Please try again with different inputs.",
                #         'trending_searches': " | ".join(
                #             newspaper.hot())})
                sources = building_source_url("", "", "")
                return render(request,
                              'quest/sources.html',
                              {'sources': sources,
                               'error': "*Your inputs resulted in 0 news sources. Showing all possible sources.",
                               'trending_searches': " | ".join(newspaper.hot())})

            # on success of getting list of sources, render sources.html with source data
            else:
                return render(request,
                              'quest/sources.html',
                              {'sources': sources,
                               'trending_searches': " | ".join(newspaper.hot())})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = URLBuilderForm()
        return render(request,
                      'quest/index.html',
                      {'form': form,
                       'trending_searches': " | ".join(newspaper.hot())})


def building_source_url(q1, q2, q3):
    # initializing variables
    # newsAPI key
    api_key = "e85d175055ae4a8a81e45640560b8e37"
    # newsAPI base url structure for getting sources
    sources_url = "https://newsapi.org/v1/sources?apiKey=" + api_key

    # append params to base url to get sources from newsAPI
    final_url = sources_url + "&country=" + \
        q1 + "&language=" + q2 + "&category=" + q3

    # return json of list of news sources
    return json.loads(urlopen(final_url).read())


def get_results(request, source_id):
    """
    This view method gets called when source link is clicked on sources.html with name url results
    ArticleForm is rendered on results.html with url_list as a hidden parameter
    """
    # initializing variables
    # newsAPI key
    api_key = "e85d175055ae4a8a81e45640560b8e37"
    # newsAPI base url structure for getting articles
    articles_url = "https://newsapi.org/v1/articles?apiKey=" + api_key

    # append params to base url to get articles from the given source
    final_url = articles_url + "&source=" + source_id
    temp_var = json.loads(urlopen(final_url).read())['articles']

    # making a list of article urls
    url_list = [v['url'] for v in temp_var]

    # create a dataframe of teh article data
    articles_df = pandas.DataFrame(temp_var)

    # ensuring display of complete contents in table cells
    pandas.set_option('display.max_rows', 1000)
    pandas.set_option('display.max_colwidth', 1000)

    # initialize the Article form with the url_list passed as a hidden field
    form = ArticleForm(
        initial={"url_list": url_list})
    return render(request,
                  'quest/results.html',
                  {'articles': articles_df.to_html(),
                   'form': form,
                   'url_list': url_list,
                   'trending_searches': " | ".join(newspaper.hot())})


def get_analysis(request):
    """
      This view method gets called on name url analysis
      If GET, list of all available sources is rendered on sources.html with blank form for emailing
      If POST, article info is extracted, analysed and rendered on analysis.html
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ArticleForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            article_id = form.cleaned_data['article_id']

            # if article id is negative, default to 0
            if article_id < 0:
                article_id = 0

            # convert the string representation of the list to list datatype
            url_list = ast.literal_eval(form.cleaned_data['url_list'])

            # get the article url from the list using article id
            article_url = url_list[article_id]

            # get the article details using newspaper library
            a = newspaper.Article(article_url)

            a.build()

            a_dict = {'url': article_url,
                      'img': a.top_image,
                      'authors': " | ".join(a.authors),
                      'title': a.title,
                      'desc': a.text,
                      'keywords': " | ".join(a.keywords),
                      'summary': a.summary,
                      'polarity': str(TextBlob(a.text).sentiment.polarity)}

            # initializing the email form with article title as subject and message containing article details
            emailform = EmailForm(
                initial={
                    'subject': a_dict['title'],
                    'message': "\n".join(
                        [
                            "Keywords: ",
                            a_dict['keywords'],
                            "Summary: ",
                            a_dict['summary'],
                            "Polarity: ",
                            a_dict['polarity'],
                            "Description: ",
                            a_dict['desc']])})

            return render(request,
                          'quest/analysis.html',
                          {'form': emailform,
                           'a_dict': a_dict,
                           'trending_searches': " | ".join(newspaper.hot())})

    # if a GET (or any other method) we'll render sources.html with complete source list
    else:
        sources = building_source_url("", "", "")
        return render(request,
                      'quest/sources.html',
                      {'sources': sources,
                       'trending_searches': " | ".join(newspaper.hot())})


def get_mail(request):
    """
    This view method gets called when send mail link is clicked on analysis.html with name url mail
    If GET, list of all available sources is rendered on sources.html with blank form for emailing
    If POST, article info is extracted, email is sent with article details and analysis
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = EmailForm(request.POST)
        # check whether it's valid
        if form.is_valid():
            subject = form.cleaned_data['subject']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            sender = 'ishijain89@gmail.com'
            recipients = ['ijain89@gmail.com', email]

            send_mail(subject, message, sender, recipients)

            # redirect to contact page after mail sent
            return HttpResponseRedirect(reverse('quest:contact'))

    # if a GET (or any other method) we'll render sources.html with complete source list
    else:
        sources = building_source_url("", "", "")
        return render(request,
                      'quest/sources.html',
                      {'sources': sources,
                       'trending_searches': " | ".join(newspaper.hot())})


def get_contact(request):
    """
    This view method gets called on name url contact
    If GET, ContactForm rendered blank for emailing
    If POST, email is sent and page redirected to home page
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = ContactForm(request.POST)
        # check whether it's valid
        if form.is_valid():
            your_name = form.cleaned_data['your_name']
            sender = form.cleaned_data['sender']
            message = form.cleaned_data['message']

            recipients = ['ijain89@gmail.com']

            send_mail(your_name, " | ".join(
                [sender, message]), sender, recipients)
            # redirect to index page after mail sent
            return HttpResponseRedirect(reverse('quest:index'))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactForm()
        return render(request,
                      'quest/contact.html',
                      {'form': form,
                       'trending_searches': " | ".join(newspaper.hot())})


def get_projects(request):
    """
    Renders projects.html page containing information of other projects done
    """
    return render(request, 'quest/projects.html',
                  {'trending_searches': " | ".join(newspaper.hot())})


def get_404(request):
    """
    Renders custom 404.html page
    """
    return render(request, 'quest/404.html',
                  {'trending_searches': " | ".join(newspaper.hot())})
