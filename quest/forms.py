from django import forms
from django.conf import settings


class URLBuilderForm(forms.Form):
    """
    The form on Index page taking inputs to build source url
    """
    q1 = forms.ChoiceField(
        label="Which country are you interested in?",
        widget=forms.RadioSelect,
        choices=settings.CHOICES_Q1)
    q2 = forms.ChoiceField(
        label="Which language are you interested in?",
        widget=forms.RadioSelect,
        choices=settings.CHOICES_Q2)
    q3 = forms.ChoiceField(
        label="Which category of news articles are you interested in?",
        widget=forms.RadioSelect,
        choices=settings.CHOICES_Q3)


class ArticleForm(forms.Form):
    """
    The form on the Results page taking the article id to build url
    """
    article_id = forms.IntegerField(label="Article ID ", initial=0)
    url_list = forms.CharField(widget=forms.HiddenInput())


class EmailForm(forms.Form):
    """
    The form taking email ID as input to send article information
    """
    subject = forms.CharField(widget=forms.HiddenInput())
    email = forms.EmailField(label='E-mail ID ')
    message = forms.CharField(widget=forms.HiddenInput())


class ContactForm(forms.Form):
    """
    Contact form for Contact page
    """
    your_name = forms.CharField(label='Your Name ', max_length=100)
    sender = forms.EmailField(label='Your E-mail ')
    message = forms.CharField(label='Your Message ')


