from django.db import models

# URL model to create database to store the shortened urls
class Urls(models.Model):
    """
    Model for squrl url database
    """
    short_url = models.SlugField(max_length=8,primary_key=True)
    input_url = models.URLField(max_length=200)
    access_date = models.DateTimeField(auto_now=True)
    visits = models.IntegerField(default=0)

def __str__(self):
    return self.input_url