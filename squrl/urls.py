from django.conf.urls import url

from . import views

app_name = 'squrl'
urlpatterns = [
    url(r'^$', views.get_squrl, name='squrl'),
    url(r'^shorten_url$', views.get_json, name='shorten_url'),
    url(r'^(?P<short_url>\w{8})$', views.redirect_url, name='redirect'),
]
