from django.contrib import admin

from .models import Urls

# Register the Urls model with UrlsAdmin options to make it accessible in admin app

class UrlsAdmin(admin.ModelAdmin):
    list_display = ('short_url','input_url','access_date', 'visits')
    ordering = ('-access_date',)

admin.site.register(Urls, UrlsAdmin)
