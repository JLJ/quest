from django import forms

class SqurlForm(forms.Form):
    """
    Contact form for squrl page
    """
    input_url = forms.URLField(label='URL ')
