from django.test import TestCase
from .models import Urls
from django.utils import timezone

class TestCalls(TestCase):
    """
    Class for squrl tests
    """
    def test_get_api_json(self):
        """
        This test checks if valid get call to api gives success after redirection to the squrl page
        """
        resp = self.client.get('/squrl/shorten_url/', format='json')
        self.assertTrue(resp.status_code, 200)

    def test_post_api_json(self):
        """
        This test checks if valid post call to api gives success after returning json response
        """
        resp = self.client.post('/squrl/shorten_url/', {'url' : 'http://www.google.com'})
        self.assertTrue(resp.status_code, 200)

    def test_call_view_loads(self):
        """
        This test checks if correct template is used to render /squrl/
        """
        response = self.client.get('/squrl/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'squrl/squrl.html')

    def test_call_view_fails_blank(self):
        """
        This test checks if redirection to /squrl/ happens on empty post request
        """
        response = self.client.post('/squrl/', {})
        self.assertRedirects(response, '/squrl/')

    def test_call_view_fails_invalid_json(self):
        """
        This test checks if redirection to /squrl/ happens on requesting with invalid json key
        """
        response = self.client.post('/squrl/', {'urla' : 'http://www.google.com'})
        self.assertRedirects(response, '/squrl/')

    def test_call_view_fails_invalid_url(self):
        """
        This test checks if redirection to /squrl/ happens on passing malformed url to json request
        """
        response = self.client.post('/squrl/', {'url' : 'googlecom'})
        self.assertRedirects(response, '/squrl/')

    def create_url(self, short_url='test', input_url='test', access_date = timezone.now(), visits = 0):
        """
        This method creates a url object
        """
        return Urls.objects.create(short_url='test', input_url='test', access_date = timezone.now(), visits = 0)

    def test_url_object_creation(self):
        """
        This test checks if the new url object created is an instance of a valid model Url
        """
        w = self.create_url()
        self.assertTrue(isinstance(w, Urls))