import json
import newspaper
import random, string
import urllib.parse as urlparse

from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, JsonResponse

from .models import Urls
from .forms import SqurlForm

@csrf_exempt
def get_json(request):
    """
    This view exposes the functionality of the url shortener via an api.
    """
    if request.method == 'POST':
        # load the request body in json_data
        json_data = json.loads(request.body)
        try:
            # The json body of the POST request must have a key 'url' containing the url to be shortened
            input_url = json_data['url']
        except KeyError:
            # Raise a 404 error if the key does not exist
            return HttpResponseRedirect(reverse('quest:404'))
        # Check if the url exists by parsing and finding th scheme. It is set to 'http' when valid.
        if urlparse.urlparse(input_url).scheme:
                try:
                    # Search if the input url exists in the database already.
                    # If it does, increment visits and return the short_url
                    temp = Urls.objects.get(input_url=input_url)
                    return JsonResponse({'shortened_url': 'http://ijain89.pythonanywhere.com/squrl/' + temp.short_url})
                except ObjectDoesNotExist:
                    # Generate the short id and create a new object in database
                    short_url = get_short_id()
                    urlobj = Urls(input_url=input_url, short_url=short_url)
                    urlobj.save()
                    return JsonResponse({'shortened_url': 'http://ijain89.pythonanywhere.com/squrl/' + short_url})

        else:
            # If url does not exist, return json with error message
            return JsonResponse({'error':'Given url does not exist'})

    # If GET is called on /shorten_url, redirect to the front-end for manual entry of url
    return HttpResponseRedirect(reverse('squrl:squrl'))

def get_squrl(request):
    """
    This view method gets called when Try sqURL link is clicked on index.html
    sqURLForm is rendered on squrl.html
    """
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = SqurlForm(request.POST)
        # check whether it is valid
        if form.is_valid():
            input_url = form.cleaned_data['input_url']
            # Check if the url exists by parsing and finding th scheme. It is set to 'http' when valid.
            if urlparse.urlparse(input_url).scheme:
                try:
                    # Search if the input url exists in the database already.
                    # If it does, increment visits and visit the short_url
                    temp = Urls.objects.get(input_url=input_url)
                    temp.visits += 1
                    temp.save()
                    return HttpResponseRedirect(temp.input_url)
                except ObjectDoesNotExist:
                    # Generate the short id and create a new object in database
                    url_list = Urls.objects.all().order_by('-access_date')
                    short_url = get_short_id()
                    urlobj = Urls(input_url=input_url, short_url=short_url)
                    urlobj.save()

                    return render(request, 'squrl/squrl.html',
                              {'url' : "/squrl/" + short_url,
                               'url_list': url_list,
                               'form': form,
                               'trending_searches': " | ".join(newspaper.hot())})
        # If form is invalid, reload the squrl page
        return HttpResponseRedirect(reverse('squrl:squrl'))
    # If GET is called on /shorten_url, redirect to the front-end for manual entry of url
    url_list = Urls.objects.all().order_by('-access_date')

    # initialize the squrl form
    form = SqurlForm()

    return render(request, 'squrl/squrl.html',
                  {'url_list': url_list,
                   'form': form,
                   'trending_searches': " | ".join(newspaper.hot())})

def redirect_url(request, short_url):
    """
    This view method gets called when GET request is called on a short_url
    or the href link is clicked on the squrl.html page
    """
    # If the object exists, get it based on the short url passed. If not found, return 404 error.
    url = get_object_or_404(Urls, pk=short_url)
    url.visits += 1
    url.save()
    # Redirect to the input_url
    return HttpResponseRedirect(url.input_url)

def get_short_id():
    """
    This method generates a unique short_id of length 8 using random function and returns the unique code
    """
    # We want to generate a code of length 8
    code_len = 8
    # create a string containing all uppercase alphabets, lowercase alphabets and digits. 26 + 26 + 10 = 62
    char_set = string.ascii_uppercase + string.digits + string.ascii_lowercase

    # This loop will keep running until a unique short_url is found
    while True:
        # Randomly pick 8 characters from the character set and join to form a string
        short_url = ''.join(random.choice(char_set) for _ in range(code_len))
        try:
            # Check for existence in the database to maintain uniqueness
            temp = Urls.objects.get(pk=short_url)
        except:
            return short_url