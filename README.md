# newsQuest - a news aggregator from multiple sources in real time based on parameters like newspaper source, category, language, country.

The idea was born when I was searching for a tool to give me category-wise country-specific news.

The first instinct was to use Web-scraping with Scrapy and Beautifulsoup but soon I realized that licensing was a major issue with scraping news websites.

Then I came across NewsAPI, newspaper, nlp and textblob libraries. Elegant solutions to my problem.

Steps to use the project:

1. On the index page, fill in the Country, Language and Category.

2. The next page will show a list of all sources retrieved with your inputs.

3. If no sources are found matching your inputs, all sources are returned.

4. Select a source by clicking the icon.

5. Next page displays the first 10 most recent articles alongwith excerpts from NewsAPI.

6. Select the Article ID on which you want to run analysis by entering in the textbox and submitting.

7. The analysis page gives the list of Keywords, Summary, Polarity and the actual article using newspaper library.

8. You can send the Analysis and the article to yourself via email!

# sqURL - a URL squeezer

The project has been built in python with Django and hosted on PythonAnywhere servers. There are 2 ways to use the project. 



A. First way:

1. Query the webservice as an API to send a request object and get a response object.

2. 'http://ijain89.pythonanywhere.com/sqURL/shorten_url' receives a POST request with JSON body containing key 'url' and value as the URL to shorten.


```
#!python
 
>>> import requests
>>> r = requests.post("http://ijain89.pythonanywhere.com/squrl/shorten_url", json = {'url' : 'http://www.google.com'})
>>> r.content
>>> b'{"shortened_url": "http://ijain89.pythonanywhere.com/squrl/RUvttjL6"}'
```


3. GET request to shortened URL returns contents of the original URL.


```
#!python

>>> import requests
>>> r = requests.get("http://ijain89.pythonanywhere.com/squrl/RUvttjL6")
>>> r.content
(returns HTML DOM)
```


4. Validatation on the URL to be shortened has been done using the urllib.parse method to check for existence of the URL given by visiting it. Another way to do this would have been through regex matching, which is essentially what Django does in the backend.


```
#!python

>>> import requests
>>> r = requests.post('http://ijain89.pythonanywhere.com/squrl/shorten_url', json = {'url':'abc'})
>>> r.content
>>> b'{"error": "Given url does not exist"}'
```




B. Second way:

1. Hit http://ijain89.pythonanywhere.com/squrl/ to reach the front end of the project.

2. You can enter the URL to be shortened into the form field. Form validation on URLField ensure well-formed URLs.

3. Get the result below along with a database view of which URLs have been stored in the database till date along with the number of times the shortened URLs have been visited.

4. When you click on any code corresponding to a URL, you are redirected to the page on the original URL.

5. If the shortened URL/original URL already exists in the database, the visit count is incremented, else a new entry is added to the database.



C. URL Shortening algorithm:

Check whether the input URL has already been entered. If it has, return the same code generated earlier. Else, choose random characters from a character set of upper case alphabets, lower case alphabets and digits to generate a code of given length (8 in this case). Before returning the shortened URL, check for existence in the database to maintain uniqueness.



D. Optimization:

1. Asynchronous catering/Multithreading - Python uses GIL to share same set of resources among multiple threads. Hence, the pseudo threading does not promise exceptional improvement as it would in, say, C Language.

2. Database Indexing - During each call to the API, the input_url is checked for validity and then searched for existence in the database. This database search can be made faster by indexing based on the column containing the input_url.

3. Modifying the function using generators and yield statement during computation of the short_id to reduce load created on the memory by for loop.



E. Configuring backend to be scalable to upto 1000 requests per second:

1. Distributing the Application and Database on different servers. That will allow usage of independent resources by calls for URL-parsing and checking URL for existence in database.

2. Caching - Recently searched URLs can be cached, which can help avoid database searches.

3. Load Balancing - Distributing the requests across multiple servers and managing the load distribution on each server using some service like nginx. This allows for spawning new servers as load increases and killing them when load is less, leading to better performance and response time as we are able to increase available resources on demand.



F. Test cases:
Some unit test cases have been written only to demonstrate unit testing using Django's TestCase Class built over python's unittest package. Please note that the test cases are not exhaustive. We can do many more unit tests to achieve better code coverage, Selenium browser tests to check front-end, integration testing to check results via queries to the database w.r.t the front-end, etc.