"""
Django settings for mysite project.
"""

import os

# Tuple of tuples for country, language and category

CHOICES_Q1 = (('au', 'Australia',), ('de', 'Germany',), ('gb', 'Great Britain',),
              ('in', 'India',), ('it', 'Italy',), ('us', 'United States',), ('all', 'All',))
CHOICES_Q2 = (('en', 'English',), ('de', 'German',), ('fr', 'French',), ('all', 'All',))
CHOICES_Q3 = (('business', 'Business',), ('entertainment', 'Entertainment',), ('gaming', 'Gaming',), ('general', 'General',), ('music', 'Music',),
              ('politics', 'Politics',), ('science-and-nature', 'Science and Nature',), ('sport', 'Sport',), ('technology', 'Technology',), ('all', 'All',))

# Building paths inside the project with os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Secret key for production

SECRET_KEY = '1m-fle$^p1yrxv(0(^77uum826&l%*!g8^8n6uyb4!*4-32cfa'

# To be turned off in production

DEBUG = False

ALLOWED_HOSTS = ['ijain89.pythonanywhere.com']


# Application definition

INSTALLED_APPS = [
    'quest.apps.QuestConfig',
    'squrl.apps.SqurlConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'mysite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Email settings

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'ishijain89@gmail.com'
EMAIL_HOST_PASSWORD = '9899732100'
EMAIL_PORT = 587
